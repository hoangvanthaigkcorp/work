import React, { useEffect } from 'react';
import { Image, View, Text, Dimensions, Platform, NativeModules, TextInput, StyleSheet, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
const NotificationScreen = () => {
    const [search, setSearch] = React.useState('');
    const showClose = () => {
        if (search == '') {
            return null;
        }
        else
            return (
                <TouchableWithoutFeedback onPress={() => {
                    setSearch('');
                }}>
                    <Image source={require('../Image/close.png')} style={{ marginTop: 15 }} />
                </TouchableWithoutFeedback>
            );
    };

    return (
        <View style={{
            marginTop: STATUSBAR_HEIGHT, ...StyleSheet.absoluteFillObject
        }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20, color: '#00C2D7' }}>Alarm History</Text>
            <View style={{ flexDirection: 'row', marginTop: 10, marginLeft: 20, height: 40,alignItems:'center' }}>
                <TextInput style={{ height:40,width: windowWidth / 4, borderRadius: 10, borderWidth: 1, borderColor: '#707070', marginRight: 20, backgroundColor: '#EFEFF4'}} placeholder="From"></TextInput>
                <TextInput style={{ width: windowWidth / 5, borderRadius: 10, borderWidth: 1, borderColor: '#707070', marginRight: 20, backgroundColor: '#EFEFF4' }} placeholder="To"></TextInput>
                <View style={{ flexDirection: 'row', width: windowWidth / 3, borderRadius: 10, borderWidth: 1, borderColor: '#707070', backgroundColor: '#EFEFF4' }}>
                    <Image source={require('../Image/search2.png')} style={{ marginTop: 10, marginLeft: 5 }} />
                    <TextInput style={{ width: windowWidth / 4 }} placeholder="" onChangeText={(text) => { setSearch(text); }} value={search}></TextInput>
                    {showClose()}
                </View>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 15, marginLeft: 20 }}>
                <TouchableOpacity>
                    <Text style={{ borderRadius: 20, width: windowWidth / 7, borderColor: '#707070', borderWidth: 1, padding: 5, marginRight: 10, backgroundColor: '#EFEFF4' }}>Critical</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text style={{ borderRadius: 20, width: windowWidth / 7, borderColor: '#707070', borderWidth: 1, padding: 5, marginRight: 10, backgroundColor: '#EFEFF4' }}>Major</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text style={{ borderRadius: 20, width: windowWidth / 7, borderColor: '#707070', borderWidth: 1, padding: 5, marginRight: 10, backgroundColor: '#EFEFF4' }}>Mirror</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text style={{ borderRadius: 20, width: windowWidth / 7, borderColor: '#707070', borderWidth: 1, padding: 5, marginRight: 10, backgroundColor: '#EFEFF4' }}>Normal</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};
export default NotificationScreen;
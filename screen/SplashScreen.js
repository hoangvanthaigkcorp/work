import * as React from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground } from 'react-native';
import styles from '../styles/styles';
import LinearGradient from 'react-native-linear-gradient';
import { Dimensions, Platform, NativeModules } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const B = (props) => <Text style={{ fontWeight: 'bold', color: 'white' }}>{props.children}</Text>
export default function SplashScreen({ navigation }) {
    return (
        <ImageBackground source={require('../Image/splash.jpg')} style={{ width: windowWidth, height: windowHeight }}>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('../Image/logo.jpg')} style={{ marginTop: 0.13 * windowHeight }} />
                <Text style={{ width: windowWidth * 0.8, textAlign: 'center', fontSize: 30, color: 'white', marginTop: 30 }}>Fire Protection Center</Text>
                <Text style={{ width: windowWidth * 0.8, textAlign: 'center', fontSize: 18, color: 'white', marginTop: 20 }}>All item have real freshness and are intended for your needs</Text>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => { navigation.navigate('SignUp') }}>
                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#4EE1CA', '#00C2D7']} style={styles.splashBtnSignUp}>
                            <Text style={{
                                textAlign: 'center',
                                color: 'white',
                                fontSize: 18,
                            }}>
                                Sign Up
                            </Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { navigation.navigate('Login') }}>
                        <View style={styles.splashBtnSignIn}>
                            <Text style={{
                                textAlign: 'center',
                                color: 'white',
                                fontSize: 18,
                            }}>
                                Sign In
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={{ width: windowWidth * 0.8, textAlign: 'center', fontSize: 14, color: '#ABBACC', marginTop: 40 }}>By joining your agree to ours <B>Terms of Service</B> and <B>Privacy Policy</B></Text>
            </View>
        </ImageBackground>
    );
}
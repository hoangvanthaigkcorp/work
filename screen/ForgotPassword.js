import * as React from 'react';
import { View, Text, Button, Image, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import styles from '../styles/styles';
import LinearGradient from 'react-native-linear-gradient';
import { Dimensions,Platform,NativeModules} from 'react-native';
import Header from '../component/Header'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
export default function ForgotPasswordScreen({ navigation }) {
  return (
    <View style={styles.loginMain}>
      <View style={{ flexDirection: 'row', width: windowWidth * 1.01, height: 70, backgroundColor: '#C9F7F1', marginTop: -STATUSBAR_HEIGHT }}>
        <TouchableOpacity onPress={() => { navigation.goBack() }}>
          <Image style={{ marginLeft: 10, marginTop: STATUSBAR_HEIGHT, marginRight:0.28*windowWidth}} source={require('../Image/back-button.jpg')} />
        </TouchableOpacity>
        <Text style={{ fontSize: 18, color: '#00C2D7', marginTop: STATUSBAR_HEIGHT }}>Forgot Password</Text>
      </View>
      <Image
        style={{width:189.79,height:80,marginLeft:0.267*windowWidth,marginTop:30,marginBottom:0.12*windowHeight}}      
        source={require('../Image/logo.jpg')}>
      </Image>
      <TextInput style={styles.textInput}  placeholder='Email'/>
      <TouchableOpacity>
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#4EE1CA','#00C2D7']} style={styles.btnLinear}>
          <Text style={{ textAlign: 'center',color:'white',fontWeight:'bold',fontSize:18 }}>
            Reset Password
        </Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => { navigation.navigate('Login') }}>
        <Text style={styles.forgotPassWordFooter}>Back to | Log in</Text>
      </TouchableOpacity>
    </View>
  );
}
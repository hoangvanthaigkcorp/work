import React from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Image, StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import styles from '../styles/styles';
const DeviceScreen = () => {
    return (
        <View style={styles.container}>
            <MapView
                style={styles.map}
                provider={PROVIDER_GOOGLE}
                region={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }}
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }}
            />
            <TouchableOpacity style={styles.btnAdd}>
                <Text style={{fontSize:48,color:'white'}}>+</Text>
            </TouchableOpacity>
        </View>
    );
};
export default DeviceScreen;
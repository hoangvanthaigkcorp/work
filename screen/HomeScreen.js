import React from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Dimensions, Image, TextInput, Text, View, TouchableOpacity } from 'react-native';
import styles from '../styles/styles';
import LinearGradient from 'react-native-linear-gradient';
const windowWidth = Dimensions.get('window').width;
const MapScreen = () => {
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        region={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
      />
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#4EE1CA', '#00C2D7']} style={styles.overlay}>
        <View style={{ flexDirection: 'row' }}>
          <Image source={require('../Image/search.png')} style={{ marginTop: 12, marginLeft: 10 }} />
          <TextInput style={{
            width: windowWidth * 0.7, color: 'white', marginLeft: 10, fontSize: 15, marginRight: 20
          }}
            placeholder="Search"
            placeholderTextColor='white'
          />
        </View>
      </LinearGradient>
      <TouchableOpacity style={styles.contact}>
        <Image
          source={require('../Image/contact.png')}
        />
      </TouchableOpacity>
    </View>
  );
};
export default MapScreen;

import * as React from 'react';
import { View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import styles from '../styles/styles';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';
import { Dimensions,Platform,NativeModules} from 'react-native';
import Header from '../component/Header'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
export default function SignUpScreen({ navigation }) {
  return (
    <View style={{ marginTop: STATUSBAR_HEIGHT }}>
      <Header title={'Sign Up'} navigation={navigation}/>
      <Image
        style={{width:189.79,height:80,marginLeft:0.267*windowWidth,marginTop:30}}      
        source={require('../Image/logo.jpg')}
      ></Image>
      <Text style={styles.signUpText}>Sign up to join</Text>
      <TextInput style={styles.textInput} placeholder='Username' />
      <TextInput style={styles.textInput} placeholder='Email'/>
      <TextInput style={styles.textInput} placeholder='Mobile Number'/>
      <TextInput style={styles.textInput} placeholder='Password'/>
      <TextInput style={styles.textInput} placeholder='Confirm Password'/>
      <TouchableOpacity>
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#4EE1CA','#00C2D7']} style={styles.btnLinear2}>
          <Text style={styles.signUpbtn}>
            Sign Up
        </Text>
        </LinearGradient>
      </TouchableOpacity>
      <View style={{ flexDirection: 'row', marginBottom: 20, marginTop: 0 }}>
        <Text style={{ marginLeft: windowWidth * 0.29, marginTop: 10, color: '#ABBACC' }}>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.signUp}>Sign In</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
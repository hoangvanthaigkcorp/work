import * as React from 'react';
import { View, Text, Button, Image, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import CheckBox from '@react-native-community/checkbox';
import styles from '../styles/styles';
import LinearGradient from 'react-native-linear-gradient';
import { Dimensions, Platform, NativeModules } from 'react-native';
import Header from '../component/Header'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
export default function LoginScreen({ navigation }) {
  const [userName, setUserName] = React.useState('');
  const [password, setPassWord] = React.useState('');
  return (
    <View style={styles.loginMain}>
      <Header title={'Sign In'} navigation={navigation}/>
      <Image
        style={{ width: 189.79, height: 80, marginLeft: 0.267 * windowWidth, marginTop: 30 }}
        source={require('../Image/logo.jpg')}
      ></Image>
      <Text style={styles.loginText}>Sign in to continue</Text>
      <TextInput style={styles.textInput} onChange={text => {
        setUserName(text);
      }}
        placeholder='Email'
      />
      <TextInput style={styles.textInput} onChange={text => {
        setPassWord(text);
      }}
        placeholder='Password'
      />
      <TouchableOpacity onPress={() => {navigation.navigate('Tab') }}>
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#4EE1CA', '#00C2D7']} style={styles.btnLinear2}>
          <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 18, color: 'white' }}>
            Sign in
        </Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => { navigation.navigate('ForgotPassWord') }}>
        <Text style={{ marginLeft: windowWidth * 0.34, color: '#ABBACC' }}>Forgot your password?</Text>
      </TouchableOpacity>
      <View style={{ flexDirection: 'row', marginTop: 0.16*windowHeight }}>
        <Text style={{ marginLeft: windowWidth * 0.30, marginTop: 10, color: '#ABBACC' }}>Don't have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
          <Text style={styles.signUp}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
import * as React from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground,ScrollView } from 'react-native';
import styles from '../styles/styles';
import { Dimensions,Platform,NativeModules,StyleSheet} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
const item = [
    { id: '53', value: 'Alarms' },
    { id: '02', value: 'Adress' },
    { id: '05', value: 'Devices' }
];
const Address = (
    { id: 'Home', value: '43 Bourke Street, Newbridge NSW 837R' }
)
const Info = [
    { id: 'Address', value: '43 Bourke Street, Newbridge NSW 837 R' },
    { id: 'Mobilie', value: '0974 234 334' },
    { id: 'DoB', value: '20.10.1987' }
]
const renderItem = () => {
    let Item = [];
    for (let index = 0; index < item.length - 1; index++) {
        Item.push(
            <View key={index} style={{borderRightWidth: 1, borderRightColor: '#9FB0C5'}}>
                <Text style={{ margin: 10, width: 100, color: '#9FB0C5' }}>
                    {item[index].id} {"\n"}{item[index].value}
                </Text>
            </View>
        )
    }
    Item.push(
        <Text key={item.length - 1} style={{ margin: 10, width: 100, color: '#9FB0C5' }}>
            {item[item.length - 1].id} {"\n"}{item[item.length - 1].value}
        </Text>
    );
    return Item;
}
const renderAddress = () => {
    return (
        <View>
            <Text style={{ marginLeft: 20, marginTop: 30, color: '#9FB0C5', fontSize: 13 }}>{Address.id}</Text>
            <Text style={{ marginLeft: 20, marginTop: 5, color: '#9FB0C5', fontSize: 13 }}>{Address.value}</Text>
        </View>
    );
}
const renderInfo = () => {
    let itemInfo = [];
    for (let index = 0; index < Info.length; index++) {
        itemInfo.push(
            <View key={index}>
                <Text style={{ marginLeft: 20, marginTop: 20, color: '#9FB0C5', fontSize: 13 }}>
                    {Info[index].id}
                </Text>
                <Text style={{ marginLeft: 20, marginTop: 5, color: '#9FB0C5', fontSize: 13 }}>{Info[index].value}</Text>
            </View>
        )
    }
    return itemInfo;
}
export default function DetailScreen({ navigation }) {
    return (
        <ScrollView style={{marginBottom:20}}  showsVerticalScrollIndicator={false}>
            <Text style={{ marginLeft: 10,marginRight:10,marginBottom:10,marginTop:STATUSBAR_HEIGHT, fontSize: 20, fontWeight: 'bold', color: '#00C2D7' }}>Member Infomation</Text>
            <View style={{ flexDirection: 'row' }}>
                <ImageBackground
                    source={require('../Image/a.jpg')}
                    imageStyle={{ borderRadius: 30 }}
                    style={{ marginTop: 10,marginLeft:10,marginBottom:10,marginRight:10, width: 60, height: 60 }}
                >
                    <TouchableOpacity>
                        <Image source={require('../Image/edit_pic.png')}
                            style={{ marginTop: 40, marginLeft: 40, width: 20, height: 20, borderRadius: 20 }}
                        />
                    </TouchableOpacity>
                </ImageBackground>
                <View style={{ marginLeft: 10 }}>
                    <Text style={{ color:'#00C2D7',marginTop: 15, fontSize: 20}}>Samuel Tanaka</Text>
                    <Text style={{ color: '#9FB0C5' }}>samuel@gmail.com</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row',marginLeft:10 }}>
                {renderItem()}
            </View>
            <View style={{ flexDirection: 'row',marginTop:30 }}>
                <Text style={{ color: '#00C2D7', marginLeft: 20, fontWeight: 'bold', fontSize: 20 }}>
                    User Information
                </Text>
                <TouchableOpacity>
                    <Image source={require('../Image/edit_profile.png')}
                        style={{ marginLeft: 180, width: 32, height: 32, borderRadius: 20 }}
                    />
                </TouchableOpacity>
            </View>
            {renderInfo()}
            <Text style={{ color: '#00C2D7', marginLeft: 20, marginTop: 30, fontWeight: 'bold', fontSize: 20 }}>Address</Text>
            {renderAddress()}
        </ScrollView>
    );
}
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './screen/LoginScreen';
import SignUpScreen from './screen/SignUpScreen';
import ForgotPasswordScreen from './screen/ForgotPassword';
import SplashScreen from './screen/SplashScreen';
import * as React from 'react';
import { Dimensions } from 'react-native';
import TabScreen from './bottom_navigation/TabScreen';
const { width, height } = Dimensions.get('screen');
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash" screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen
          name="Tab" component={TabScreen}
        />
        <Stack.Screen
          name="Splash" component={SplashScreen}
        />
        <Stack.Screen
          name="Login" component={LoginScreen}
        />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
        <Stack.Screen name="ForgotPassWord" component={ForgotPasswordScreen}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

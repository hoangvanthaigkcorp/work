import { Dimensions, Platform, NativeModules, StyleSheet } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
const styles = StyleSheet.create({
    loginMain: {
        marginTop: STATUSBAR_HEIGHT,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        alignSelf: 'center'
    },
    loginText: {
        marginLeft: 0.304 * windowWidth,
        marginTop: 20,
        marginBottom: 0.043 * windowHeight,
        color: 'black',
        fontSize: 20,
    },
    signinNote: {
        marginLeft: 20,
        marginTop: 10,
        color: '#ABBACC',
        fontSize: 13,
        marginBottom: 30
    },
    signinNote1: {
        marginLeft: 20,
        marginTop: 10,
        color: '#ABBACC',
        fontSize: 13,
    },
    textInput: {
        borderWidth: 1,
        marginTop: 20,
        marginLeft: 20,
        padding: 5,
        width: windowWidth * 0.92,
        borderColor: '#9C9C9C',
        borderRadius: 10,
    },
    signUp: {
        fontWeight: 'bold',
        marginTop: 10,
        color: '#ABBACC'
    },
    signUpText: {
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 0,
        color: 'black',
        fontSize: 20,
    },
    forgotPassWordFooter: {
        marginLeft: windowWidth * 0.4,
        marginTop: windowHeight * 0.3,
        textAlign: 'center',
        color: '#ABBACC'
    },
    btnLinear: {
        borderRadius: 20,
        width: windowWidth * 0.92,
        height: 40,
        padding: 8,
        margin: 20,
    },
    btnLinear2: {
        borderRadius: 20,
        width: windowWidth * 0.92,
        height: 45,
        padding: 10,
        margin: 20,
    },
    signUpbtn: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    },
    splashBtnSignIn: {
        width: windowWidth * 0.38,
        height: 50,
        marginTop: 0.26 * windowHeight,
        padding: 12.5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'white',
    },
    splashBtnSignUp: {
        borderRadius: 10,
        width: windowWidth * 0.38,
        height: 50,
        padding: 12.5,
        marginTop: 0.26 * windowHeight,
        marginRight: 15
    },
    searchLinear: {
        borderRadius: 20,
        width: windowWidth * 0.8,
        height: 40,
        margin: 20,
    },
    overlay: {
        position: 'absolute',
        top: STATUSBAR_HEIGHT * 2,
        borderRadius: 40,
        width: windowWidth * 0.8,
        height: 40,
    },
    contact: {
        position: 'absolute',
        bottom: 100,
        left: 0,
        height: 40,
    },
    btnAdd: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 60,
        backgroundColor: '#00C2D7',
        borderRadius: 100
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        flex:1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
});
export default styles;
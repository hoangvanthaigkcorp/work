import * as React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text, Button, Image, TouchableOpacity } from 'react-native';
import { Dimensions, Platform, NativeModules } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBarManager.HEIGHT;
const Header= (props) => {
    return (
        <View style={{ flexDirection: 'row', width: windowWidth * 1.01, height: 70, backgroundColor: '#C9F7F1', marginTop: -STATUSBAR_HEIGHT }}>
            <TouchableOpacity onPress={()=>{props.navigation.navigate('Splash')}}>
                <Image style={{ marginLeft: 10, marginTop: STATUSBAR_HEIGHT,marginRight:0.35*windowWidth }} source={require('../Image/back-button.jpg')} />
            </TouchableOpacity>
            <Text style={{flexDirection:'row',textAlign:'center',fontSize: 18, color: '#00C2D7', marginTop: STATUSBAR_HEIGHT,textAlign: "center"}}>{props.title}</Text>
        </View>
    );
}
export default Header;
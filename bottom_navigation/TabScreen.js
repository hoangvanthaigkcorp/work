import { createStackNavigator } from '@react-navigation/stack';
import DetailScreen from '../screen/DetailScreen';
import HomeScreen from '../screen/HomeScreen';
import PlaceScreen from '../screen/PlaceScreen'
import DeviceScreen from '../screen/DeviceScreen'
import NotificationScreen from '../screen/NotificationScreen'
import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image, View, TouchableWithoutFeedback,Text } from 'react-native';

//bottom navigation
const HomeStack = createStackNavigator();
const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={HomeScreen} options={{
        headerShown: false,
      }} />
    </HomeStack.Navigator>
  );
}
const PlacesStack = createStackNavigator();
const PlacesStackScreen = () => {
  return (
    <PlacesStack.Navigator>
      <PlacesStack.Screen name="Home" component={PlaceScreen} options={{
        headerShown: false,
      }} />
    </PlacesStack.Navigator>
  );
}
const DevicesStack = createStackNavigator();
const DevicesStackScreen = () => {
  return (
    <DevicesStack.Navigator>
      <DevicesStack.Screen name="Home" component={DeviceScreen} options={{
        headerShown: false,
      }} />
    </DevicesStack.Navigator>
  );
}
const NotificationsStack = createStackNavigator();
const NotificationsStackScreen = () => {
  return (
    <NotificationsStack.Navigator>
      <NotificationsStack.Screen name="Home" component={NotificationScreen} options={{
        headerShown: false,
      }} />
    </NotificationsStack.Navigator>
  );
}
const AccountStack = createStackNavigator();
const AccountStackScreen = () => {
  return (
    <AccountStack.Navigator>
      <AccountStack.Screen name="Home" component={DetailScreen} options={{
        headerShown: false,
      }} />
    </AccountStack.Navigator>
  );
}

//bottom tab
const renderIconTabBar = (props, route) => {
  let icon;
  const getHeight = route === props.state.routes[props.state.index] ? 64 : 36;
  const linkIcon = '../Image/';
  if (route.name === 'Home') {
    icon = <Image
      resizeMode="contain"
      style={{ height: getHeight ,alignSelf:'center'}}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'home_click.png') : require(linkIcon + 'home1.png')}
    />;
  } else if (route.name === 'Places') {
    icon = <Image
      resizeMode='contain'
      style={{ height: getHeight ,alignSelf:'center'}}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'places_click.png') : require(linkIcon + 'places1.png')}
    />;
  } else if (route.name === 'Devices') {
    icon = <Image
      resizeMode="contain"
      style={{ height: getHeight ,alignSelf:'center'}}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'devices_click.png') : require(linkIcon + 'devices1.png')}
    />;
  } else if (route.name === 'Notifications') {
    icon = <Image
      resizeMode="contain"
      style={{ height: getHeight ,alignSelf:'center'}}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'noti_click.png') : require(linkIcon + 'noti1.png')}
    />;
  } else if (route.name === 'Account') {
    icon = <Image
      resizeMode='contain'
      style={{ height: getHeight ,alignSelf:'center'}}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'account_click.png') : require(linkIcon + 'account1.png')}
    />;
  }
  return icon;
};
const renderLabelTabBar = (props, route) => {
  let label;
  const getFont = route === props.state.routes[props.state.index] ? 14 : 10;
  const getMarinTop = route === props.state.routes[props.state.index] ? -10 : -5;
  const styleLabel = { color: '#fff', fontSize: getFont,textAlign:'center',marginTop:getMarinTop };
  if (route.name === 'Home') {
    label = <Text style={styleLabel}>Home</Text>;
  } else if (route.name  === 'Places') {
    label = <Text style={styleLabel}>Places</Text>;
  } else if (route.name  === 'Devices') {
    label = <Text style={styleLabel}>Devices</Text>;
  } else if (route.name  === 'Notifications') {
    label = <Text style={styleLabel}>Notifications</Text>;
  } else if (route.name  === 'Account') {
    label = <Text style={styleLabel}>Account</Text>;
  }
  return label;
};
const RenderTabBarNavigation = (prop) => {
  const styleActive = {
    justifyContent: 'space-evenly',
  };
  return (
    <View style={{
      marginBottom: -10,
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      backgroundColor: '#00C2D7'
    }}>
      {
        prop.state.routes.map((route, index) => {
          return (
            <TouchableWithoutFeedback
              onPress={() => {
                prop.navigation.navigate(route.name);
              }}
              key={index}
            >
              {
                prop.state.index === index ?
                  <View
                    style={[prop.state.index === index && styleActive, {position: 'relative', top: -15 }]}>
                    {renderIconTabBar(prop, route)}
                    {renderLabelTabBar(prop,route)}
                  </View>
                  :
                  <View
                    style={[prop.state.index === index && styleActive,]}>
                    {renderIconTabBar(prop, route)}
                    {renderLabelTabBar(prop,route)}
                  </View>
              }
            </TouchableWithoutFeedback>
          );
        })
      }
    </View>
  );
};
const Tab = createBottomTabNavigator();
const TabScreen = () => {
  return (
    <Tab.Navigator tabBar={prop => <RenderTabBarNavigation {...prop} />}>
      <Tab.Screen name="Home" component={HomeStackScreen} />
      <Tab.Screen name="Places" component={PlacesStackScreen} />
      <Tab.Screen name="Devices" component={DevicesStackScreen} />
      <Tab.Screen name="Notifications" component={NotificationsStackScreen} />
      <Tab.Screen name="Account" component={AccountStackScreen} />
    </Tab.Navigator>
  );
};
export default TabScreen;